<?php
/**
 * Tento skript vytvoří webové stránky z php ksriptů
 * PHP je zde nástrojem buildovacím, sestavovacím a nikoliv
 * webovým skriptovacím jazykem,
 * na serveru to již běží bez PHP
 */
namespace pruga;

use function pruga\utils\{path,
            get_server_path_info,
            glob_recursive_files,
            relativePath,
            change_file_extension
            };

use const pruga\utils\NL;

use pruga\exceptions\{UndefinedVariableError};

$doc = file_get_contents(change_file_extension(__FILE__ , ".docopt"));

$args = \Docopt::handle($doc);

// date_default_timezone_set('UTC+1');

// -------------------------------
error_reporting(E_ALL);


function exception_error_handler($severity, $message, $file, $line): void {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    
    $code = 2;
    throw new UndefinedVariableError("odchycený warning: " . $message, $code /*, $severity, $file, $line*/);
}

/** nezapínat, bo potom skončí mkdir na tom, že jestvuje už adresář **/
set_error_handler('pruga\exception_error_handler', E_WARNING);
    

// ------------------------------------

function main(\Docopt\Response $args) {
    // var_dump($args);
    

    // $configFile = find_file('config.php');

    include_once __DIR__ . '/config.php';

    define('RELEASE', isset($args["--release"]));
    // define('DEBUG', $args["--release"]);
    // var_dump($args);exit();
    if (RELEASE) {
        echo "režim RELEASE" . NL;
    }
    else{
        echo "režim DEBUG" . NL;
    }
    
    try {
        foreach (RouteBuilder::get_index_routes($args) as $route) {
            //    echo $route . NL;
                $html = $route->buildPage();
                if(!RELEASE){
                    echo "----------------------" . NL;
                    echo $html . NL . NL;
                }
               
               $route->file_put_contents($html);
            }
    } catch (UndefinedVariableError $err) {
       echo "\n";
       echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
       echo $err;
       exit();
    }
    
    
}

main($args);