<?php


const LOG_DIRECTORY = "log/pages";
const PUBLIC_DIRECTORY = "public";
const PAGES_DIRECTORY = "pages";
const TEMPLATES_DIRECTORY = "templates";
// const ASSETS_DIRECTORY = "assets";


define('API_MAPY_CZ_LOADER', 'http://api4.mapy.cz/loader.js');

const APP_NIM = 'app.nim';
const APP_JS = 'app.js';
const APP_OUT = 'app.out';
const META_PHP = 'meta.php';
const INDEX_HTML = 'index.html';
const INDEX_PHTML = 'index.phtml';
const INDEX_PMD = 'index.pmd';
const INDEX_MD = 'index.md';
const INDEX = 'index';
const LAYOUT_PHTML = 'layout.phtml';