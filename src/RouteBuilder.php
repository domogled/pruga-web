<?php

namespace pruga;

use function pruga\utils\{path,
    get_server_path_info,
    glob_recursive_files,
    relativePath,
    change_file_extension,
    create_logger,
    find_parent,
    paths2root
    };

use const pruga\utils\NL;

// use Monolog\Logger;
// use Monolog\Handler\StreamHandler;
// use Monolog\Handler\PHPConsoleHandler;
// use Monolog\Handler\FirePHPHandler;
// use Monolog\Formatter\JsonFormatter;

function get_index_paths(string $rootDir): \Generator
{
    /**
     * provede glob na adresář a vrací routy
     * vrací jen ty routy, kde v cestě její jestvuje soubor index.{html,md}
     */

    $paths = glob_recursive_files($rootDir, "index.{phtml,html,md}", GLOB_BRACE);

    foreach ($paths as $path) {
        //    echo $path . NL;
        yield relativePath(path:$path, base:$rootDir);
    }
}

class RouteBuilder
{

    private \Monolog\Logger $__logger;
    // private WebBuilder $web;
    private string $__pages_dir = PAGES_DIRECTORY;
    private string $__template_dir = TEMPLATES_DIRECTORY;
    private string $__public_dir = PUBLIC_DIRECTORY;
    private string $__log_dir = LOG_DIRECTORY;

    // public static function getInstance(string $route, \Docopt\Response $args): RouteBuilder
    // {

    // }

    public function __construct(
        // WebBuilder $web,
        private string $__route,
        private \Docopt\Response $__args
    ) {
        $logDir = $__args["--log-dir"] ?? LOG_DIRECTORY;
        $logFile = path($logDir, $__route, 'build.log');
        $logger = create_logger($logFile);
        $logger->info("vytvářím routu ", [$__route]);
        $logger->info("zápis o průběhu sestavení stránek zapíšu do souboru $logFile. ", [$logFile, $__route]);
        $this->__logger = $logger;
    }

    public static function get_index_routes(\Docopt\Response $args): \Generator
    {
        /**
         * kvůli namespace nejde použít get_index_routes()
         * taky aby to šlo přes Autoload tak můžu použít Route::get_index_routes()
         */

        $pages_dir = $args["--pages-dir"] ?? PAGES_DIRECTORY;
        // $public_dir = $args["--public-dir"] ?? PUBLIC_DIRECTORY;

        $paths = get_index_paths($pages_dir);

        foreach ($paths as $path) {
            $route = dirname($path);
            //    echo $path . NL;

            yield new RouteBuilder(__route:$route, __args:$args);
        }

    }

    public function __toString(): string
    {

        return "ROUTE {$this->__route} AS {{$this->__route}->toSlug()}";
    }

    private function __templatePath(string $name): string
    {
        return path($this->__template_dir, $name);
    }

    private function get_meta_files(string $name): \Generator
    {

        $defaultMeta = $this->__templatePath(META_PHP);
        if (!is_file($defaultMeta)) {
            throw new \Exception("Nejestvuje výchozí meta soubor $defaultMeta.");
        }
        yield $defaultMeta;

        $namedMeta = $this->__templatePath("meta-$name.php");
        if (is_file($namedMeta)) {
            yield $namedMeta;
        }

        $metaPaths = array_reverse(iterator_to_array(paths2root($this->__route, root:$this->__pages_dir)));
        // // var_dump($metaPaths);

        foreach ($metaPaths as $metaDir) {
            $metaFile = path($metaDir, META_PHP);
            if (is_file($metaFile)) {
                $this->__logger->info("našel jsem meta soubor $metaFile", [$metaFile]);
                yield $metaFile;
            } else {
                $this->__logger->warning("nejestvuje meta soubor $metaFile", [$metaFile]);
            }

        }
        // return;
    }

    private function include_with_meta(string $source)
    {
        /**
         * tady se vkláfdají šablony
         * před každou šablonou se vloží všechny meta a meta-$name soubory
         *
         */

        $info = pathinfo($source);
        $name = $info['filename'];

        foreach ($this->get_meta_files($name) as $metaFile) {
            include $metaFile;
        }
        include $source;
    }

    public function __call(string $name, array $args)
    {
        /****
         * pomocí $this->name() dosáhneme vložení šablony
         * hledáme šablonu name v adresáři routy
         * a ve všech adresářích nadřazených až do kořene
         * a když nenajdeme
         * tak zkusíme, zda není v adresáři templates 
         */
        $fileName = "$name.phtml";

        foreach (paths2root($this->__route, root:$this->__pages_dir) as $route) {

            $filePath = path($route, $fileName);

            if (is_file($filePath)) {
                $this->__logger->info("vložím šablonu $filePath");
                $this->include_with_meta($filePath);
                return;
            }

        }

        $defaultFile = path($this->__template_dir, $fileName);
        if (is_file($defaultFile)) {
            $this->__logger->info("vložím výchozí šablonu $defaultFile");
            // include $defaultFile;
            $this->include_with_meta($defaultFile);
            return;
        }

        throw new \Exception("Nejestvuje soubor $fileName.");
        // echo "\n<!-- ************** Nejestvuje šablona $fileName. ************** -->\n";
        return;
    }

    private function index()
    {

        $routePath = path($this->__pages_dir, $this->__route);

        $index_html = path($routePath, INDEX_HTML);
        $index_md = path($routePath, INDEX_MD);

        if (is_file($index_html) && is_file($index_md)) {
            throw new \Exception("Index ve dvou formátech, jestvuje $index_html aj $index_md.");
        }

        if (is_file($index_html)) {
            $this->include_with_meta($index_html);
        } elseif (is_file($index_md)) {
            ob_start();

            // include $index_md;
            $this->include_with_meta($index_md);

            $mdContent = ob_get_contents();

            ob_end_clean();

            $parser = new \Parsedown();
            echo $parser->text($mdContent);
        } else {
            echo "<p>stránka " . $this->__route . " ještě nemá vytvořený žádný obsah.</p>";

        }

    }

    public function buildPage()
    {

        $this->__logger->info("build {$this->__pages_dir} => {$this->__public_dir}", [$this->__pages_dir, $this->__public_dir]);

        $layoutPath = find_parent($this->__pages_dir, $this->__route, LAYOUT_PHTML);

        $this->__logger->info("vložím layout $layoutPath", [$layoutPath]);

        // --------------------- META DATA

        // --------------------- LAYOUT

        ob_start();

        include $layoutPath;

        $pageContent = ob_get_contents();

        ob_end_clean();

        return $pageContent;
    }

    private function add_app_script()
    {

        $jsFile = path($this->__pages_dir, $this->__route, APP_JS);
        $nimFile = path($this->__pages_dir, $this->__route, APP_NIM);

        $isApp = is_file($nimFile) || is_file($jsFile);

        if ($isApp) {
            // include $appFile;

            $scriptDir = path($this->__public_dir, $this->__route);
            
            if (!is_dir($scriptDir)) {
                mkdir($scriptDir, 0760, true);
            }

            $scriptFile = path($scriptDir, APP_JS);

            if (is_file($jsFile)) {
                if (!copy($jsFile, $scriptFile)) {

                    $this->__logger->error("selhalo kopírování $jsFile => $scriptFile", [$jsFile, $scriptFile]);
                    throw new Exception("failed to copy $appFile to $script_dir.\n", 1);

                } else {
                    $this->__logger->info("zkopírováno $jsFile => $scriptFile", [$jsFile, $scriptFile]);

                    // echo '<script type="text/javascript">' . . '</script>';
                }
            }

            if (is_file($nimFile)) {

                $command = "pruga-js --force --out:$scriptDir --sourcemap $nimFile";
                $this->__logger->info("spustím kompilaci aplikace", [$command]);

                if ($this->__args['--compile']) {

                    // echo $command;
                    $output = null;
                    $retval = null;
                    exec($command, $output, $retval);
                    $output = join("\n", $output);
                    $logFile = path($this->__log_dir, $this->__route, APP_OUT);

                    $logDir = dirname($logFile);
                    if (!is_dir($logDir)) {
                        mkdir($logDir, 0777, true);
                    }
                    file_put_contents($logFile, $output);

                    if ($retval != 0) {
                        throw new \Exception("Error command $command with status $retval and output:\n$output\n", 1);

                    }
                    // exec("ls", $output, $retval);
                    // echo "Returned with status $retval and output:\n";
                    // print_r($output);
                } else {
                    $this->__logger->info("vypnuta kompilace z $nimFile nebude sestaven javascript");
                    echo "\n<!-- ************** vypnuta kompilace z $nimFile nebyl sestaven javascript.

                    $ $command
                    ************** -->\n";
                }

            }

            $scriptFilePath = relativePath(path: $scriptFile, base: $this->__public_dir);
            echo '<script type="text/javascript" src="' . $scriptFilePath . '"></script>';

        } else {
            echo "\n<!-- ************** Nejestvuje aplikace $nimFile nebo $jsFile. ************** -->\n";
        }
    }

    public function file_put_contents(string $content)
    {

        $destination = path($this->__public_dir, $this->__route, INDEX_HTML);
        $destinationDir = dirname($destination);

        if (!is_dir($destinationDir)) {
            mkdir($destinationDir, 0700, $recursive = true);
        }

        $this->__logger->info("zapíšu html stránku $destination", [$destination]);

        file_put_contents($destination, $content);

    }

}
