#!/usr/bin/env fish

# Toto je úplně nejjednodušší 'build system'
# obyčejný shelový skript

# při každém spuštění vyčistíme okno terminálu
clear

# vypíšeme nějakou veselou zprávu
# echo "Toto je velice jednoduchý avšak velice mocný 'build system'"

echo "Пруга идзе !!!"

# v shellu se dá programovat

# ale pokud nemáš rád shell, tak používej jiný jazyk

# napiš si třeba skript v PHP
# php ./devel.php $1
# pruga-build clear
php ./devel.php --pages-dir ./pages   # --compile
# nim devel
# ./vendor/bin/pest ./src

# convert -resize x16 -gravity center -crop 16x16+0+0 src/pages/favicon.jpg -flatten -colors 256 -background transparent src/pages/favicon.ico
