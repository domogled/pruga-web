


when defined(c) and not defined(Nimdoc):
    {.error: "This module only works on the c platform".}

import karax / [karaxdsl, vdom]

import std/strformat
import pruga/http/html/terminal

# when defined(js):

import std/dom
# import pkg/jswebsockets
import pkg/karax/jwebsockets
import pruga/http/uri/schema
import pruga/http/html/terminal

const ENTER = "Enter"


proc terminalClient*(event: Event) =

    let ws_protocol = if window.location.protocol == $HTTP: $WS else: $WSS 

    let ws_location = &"{ws_protocol}//{window.location.host}/ws"

    var socket = newWebSocket(ws_location)

    socket.onOpen = proc (e:MessageEvent) =
        let cmd = "echo $USER"
        echo "sent: ", cmd
        socket.send cmd

    socket.onMessage = proc (e:MessageEvent) =
        echo("received: ",e.data)
        document.getElementById(ID_TERMINAL_OUTPUT).innerHTML.add e.data
        # socket.close(StatusCode(1000),"received msg")
    # socket.onClose = proc (e:CloseEvent) =
    #     echo("closing: ",e.reason)

    proc onCommandInput(event: KeyboardEvent) =


        # case event.keyCode
        case $event.key
        # of 13:
        of ENTER:
            # window.alert &"odeslano jest {event.key} {event.target.value}"
            let command = event.target.value
            socket.send command
            event.target.value = ""
        else:
            discard
            # document.getElementById(ID_TERMINAL_OUTPUT).innerHTML.add event.key

    let element = document.getElementById(ID_TERMINAL_INPUT)
    # element.addEventListener($KeyUp, onCommandInput)
    # .addEventListener($KeyUp, onCommandInput)

# proc main() =

#     setRenderer(terminalClient, root = APP_ID)

when isMainModule:
    echo "pruga skript"
    # window.onLoad = terminalClient
    window.document.addEventListener($Load, terminalClient)