try:
  var ws = await newWebSocket(request)
  await ws.send("Dobro došli u websocket terminal")
  while ws.readyState == Open:
    let packet = await ws.receiveStrPacket()
    # await ws.send(packet)
    info &"dotaz: {packet}"
    try:
      # let output = doCmdEx packet
      # await ws.send output
      # https://nim-lang.org/docs/osproc.html#waitForExit%2CProcess%2Cint
      # WARNING: Be careful when using waitForExit for processes created without poParentStreams because they may fill output buffers, causing deadlock.
      # jenže použití poParentStreams způsobí,
      # že se výstup vypíše do konzole na serveru
      # poEvalCommand musí být jinak nerozpozná proměnné prostředí $USER zůstane jako $USER
      const opts = { poEvalCommand, poUsePath} #poStdErrToStdOut
      const TIMEOUT = 30
      let process = startProcess(command = packet, options = opts)
      let outputStream = process.outputStream()
      let errorStream = process.outputStream()
      let exCode = process.waitForExit(TIMEOUT)
      if exCode == QuitSuccess:
        info &"{packet} exit with code {exCode}"
      else:
        error &"{packet} exit with code {exCode}"

      let msg = %* {"command": packet,
            "code":  exCode,
            "stdout": outputStream.readAll(),
            "stderr": errorStream.readAll()
            }
      
      await ws.send $msg
      # for line in outputStream.lines():
      #   info line
      #   await ws.send $notification(line, COLORS.info)

      # for line in errorStream.lines():
      #   error line
      #   await ws.send $notification(line, COLORS.warning)
    except Exception as err: 
      await ws.send $notification(err.msg, COLORS.danger)

except Exception as err:
  error "chyba při dotazu na websocket /ws", err.msg