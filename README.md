# pruga-web

Toto je jednoduchý generátor statických stránek, který využívá síly jazyka PHP v jeho původní jednoduchosti šablonovacího jazyka.

## statický web

Spuštěním skriptu `./bin/build-pages.php` se vytvoří adresář *public* a v něm najdeš html stránky. Tento adresář umístíš na webový server. Na serveru už žádné *php* není potřeba. Web je statický. 

## obsah webu

Dobře, ale z čeho se webové stránky vytvoří? V adresáři `pages` vytvoříš soubor `index.phtml` a v něm bude obsah hlavní stránky. V podadresářích v souborech `index.phtml` budou podstránky:

./pages/index.phtml => http://localhost
./pages/blog/index.phtml => http://localhost/blog
./pages/blog/edit/index.phtml => http://localhost/blog/edit

Sestavovací skript `./src/build-pages.php` nejprve projde adresář `pages` a hledá všechny soubory `index.phtml`. Každý jeden takový soubor pak zpracuje jako *routu*. Výsledkem zpracování je příslušný soubor `index.html` v adresáři `public`. Http servery bývají nastavené tak, že soubor s názvem `index.html` nabízejí jako výchozí obsah. Máme tak pěkné url adresy, kvůli kterým se dělají jinde různá nastavení.

> V adresáři `pages` mohou být další soubory pro PHO server GET.php POST.php a taky nim a javascript soubory pro fronted.
## šablonovací a sestavovací systém

PHP samo o sobě je šablonovacím systémem. Toho zhusta využíváme. Proto jsou zdrojové soubory s příponou `.phtml`. Lze zde použít plnou sílu jazyka PHP, ale čiň tak prosím s rozumem. Filosofie tohoto řešení je taková, že v souborech `index.phtml` se vytváří vlastní obsah stránky. Tím myslím tu část, kterou by jste dali třeba do elemntu `<article>`, či do `<div id="main">`. V `index.phtml` nebude menu, sidebar, hlavička, ani patička.

Sestavovací skript nezačne zpracovávat například soubor `pages/blog/category/index.phtml` hned, ale nejdříve se podívá, zda jestvuje soubor `pages/blog/category/layout.phtml`. Když tento není (a doporučuji ho zbytečně nevytvářet), hledá se layout o adresář výše:
 `pages/blog/layout.phtml`
 `pages/layout.phtml`
 a když ani ten není, pak vezme výchozí
 `templates/layout.phtml`.

Tímto způsobem je možné nastavit vlastní layout pro celý web v kořenovém adresáři a jiný layout nastavit pro všechny stránky

TODO: obrázek jak to funguje


## bloky

To čemu se v jiných šablonovacích systémech říká bloky je zde vyřešeno takto. Pomocí `<?= $this->bocni_menu() ?>` vložím do stránky boční menu. Děje se tak pomocí magie v jediné třídě, kterou tento systém má `class RouteBuilder::__call()`. Jinak jsem se snažil řešit vše pomocí čistých i nečistých funkcí, ale v tomto případě mi síla OOP přišla vhod.
Takže po zavolání `<?= $this->bocni_menu() ?>` v šabloně se uděje to, že se hledá v témže adresáři (téže routě) soubor `bocni_menu.phtml`. A když není, postupuje se obdobně jako s `layout.phtml`. Hledá v nadřezených adresářích až do kořene webu `pages/bocni_menu.phtml` a když ani tam neuspěje, zkusí se podívat do `templates/bocni_menu.phtml`. 

Toto je ve skutečnosti velice flexibilní systém a přítom jednoduchý systém. A také přehledný.

## meta

V šablonách jsou dostupné proměné, které se nastaví v souborech `meta.php`. Sestavovací skript ve skutečnost dělá to, že před vložením šablony načte soubory `meta.php` a to v opačném pořadí, než hledá šablony. 

TODO: obrázek je víc, než kopa textu

