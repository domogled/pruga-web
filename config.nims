#!/usr/bin/env nim
mode = ScriptMode.Verbose
# mode = ScriptMode.Silent

## compile cli programs, default from src/cli/{command}) and save it in bin/pruga-{command}

switch("app", "console")
switch("colors", "on")

# switch("define", "PROJECT_DIR:" & thisDir())

switch("cc", "clang")

import pruga/args
import pruga/build/project
import pruga/build/route
import pruga/cli/parser
# import pruga/cli/script
import std/distros
import std/os
import std/strformat
import std/strutils

const (BASEDOMAIN, SUBDOMAIN, PATH) = getCurrentDir().lastPathPart().parseDomain()
const DOMAIN = &"{SUBDOMAIN}.{BASEDOMAIN}"

task info, "informace o systému":
    # Architectures.
    if defined(amd64):
        echo "Architecture is x86 64Bits"
    elif defined(i386):
        echo "Architecture is x86 32Bits"
    elif defined(arm):
        echo "Architecture is ARM"
    else:
        echo "neznámá architektura"

    # Operating Systems.
    if defined(linux):
        echo "Operating System is GNU Linux"
    elif defined(windows):
        echo "Operating System is Microsoft Windows"
    elif defined(macosx):
        echo "Operating System is Apple OS X"
    else:
        echo    "neznámý operační systém"

    # Distros.
    if detectOs(Ubuntu):
        echo "Distro is Ubuntu"
    elif detectOs(ArchLinux):
        echo "Distro is ArchLinux"
    elif detectOs(Manjaro):
        echo "Distro is Manjaro"
    elif detectOs(Debian):
        echo "Distro is Debian"
    else:
        echo    "neznámá distribuce"



let webPath = thisDir() / PUBLIC_DIRECTORY
let buildPath = thisDir() / BUILD_DIRECTORY
const INCUDE_BEFORE_HEADER_HTML = "include-before-body.html"
const MENU_JSON = "menu.json"

task  pages, "sestaví webové stránky":

    let php_builder = BIN_DIRECTORY / $build_pages_php
    exec &"php {php_builder}"

task  clear, "odstrání adresáře build a public, ve ktrých je generovaný obsah":

    exec &"pruga-build clear"

task  apps, "zkompiluje, či zkopíruje skripty aplikací":

    exec &"pruga-build apps"

task  favicon, "kopíruje favicon":

    exec &"pruga-build favicon"

task compile_server, "kompiluji server":

    exec "pruga-build server"

task start_server, "kompiluji server":
    withDir PUBLIC_DIRECTORY:
        exec "pruga-server php localhost 8087"
        # exec "php -S localhost:8087"
    

# ------------------------------gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg

# na odstřel asi ne, probráno

# ------------------------------------------------------------------
# PRO VSECHNA

proc ssh(command: string) =

    exec &"""ssh root@{DOMAIN} "{command}"
    """




task css, "sestavím styly":

    echo "sestavím styly"
    exec &"pruga-build css from {webPath}/style.css to {buildPath}/styles/main.css"




task laravel, "init laravel projekt":

    exec "composer require barryvdh/laravel-debugbar --dev"

task nim, "instauje nim":

    exec "curl https://nim-lang.org/choosenim/init.sh -sSf | sh"

task sync, "synchronizace":   
    
    # exec "rsync -a ./bin/ root@domogled.com:/pruga/bin/"
    ssh &"mkdir -p /pruga/server/web/{DOMAIN}/public"
    exec &"rsync -a --owner={WWW_USER} --group={WWW_GROUP} ./public/ root@domogled.com:/pruga/server/web/{DOMAIN}/public"



task nginx_create, "create nginx config":

    let nginxFile = &"etc/nginx/sites-available/{DOMAIN}.nginx"

    rmFile nginxFile

    exec &"pruga-files etc/nginx/sites-available/php-fpm.nginx {nginxFile} --domain:{DOMAIN}"
    

task nginx_sync, "create nginx config":

    exec "rsync -a --owner=root --group=root ./etc/nginx/sites-available/ root@domogled.com:/etc/nginx/sites-available/"


task enable, "enable nginx server on remote server":

    let command = &"pruga-nginx enable {DOMAIN}.nginx;nginx -t;nginx -s reload"

    # ssh "ls -al /etc/nginx/sites-available/"
    ssh command
    
task httpTest, "test http spojeni":

    exec &"pruga-http test GET 301 http://{DOMAIN} https://{DOMAIN}"
    exec &"pruga-http test GET 301 http://{DOMAIN}/index.html https://{DOMAIN}/index.html"
    exec &"pruga-http test GET 200 https://{DOMAIN}"
    exec &"pruga-http test POST 200 https://{DOMAIN}"
    
task certbot, "certifikáty":

    let domains = if SUBDOMAIN == "www": &"-d {BASEDOMAIN} -d {DOMAIN}" else: &"-d {DOMAIN}"
    ssh &"certbot {domains};nginx -s reload"
    exec "mkdir -p ./etc/nginx/sites-available"
    exec &"rsync -a root@domogled.com:/etc/nginx/sites-available/{DOMAIN}.nginx ./etc/nginx/sites-available/{DOMAIN}.nginx"

task test, "pest php tests":
    
    exec "./vendor/bin/pest ./src"


task devel, "vývojový task":
   pagesTask()
    